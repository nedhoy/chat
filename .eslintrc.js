module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "indent": [
            "error",
            "tab",
            {
                "SwitchCase": 1,
            },
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-mixed-spaces-and-tabs": [
            "error",
            "smart-tabs",
        ],
        "quotes": [
            "error",
            "double"
        ],
    }
};
