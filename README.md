# IRC
This project is an [IRC](https://wikipedia.org/wiki/Internet_Relay_Chat) web client.

## Building
If you don't have [npm](https://npmjs.com), the Node.js package manager, you'll need to get that first.

```sh
npm install
npm run build
```

## Production
For production, you should use

```sh
npm run build:prod
```

which will minify the JavaScript and quiet debug prints.

## Customise
The [config.js](./src/config.js) file can be edited to customise the client.

## Technologies

* The view is written using [React](https://facebook.github.io/react) and JSX.
* State is managed using the [Redux](http://redux.js.org) pattern.
* Static type checking is done with [Flow](https://flowtype.org/).

## Licence

This project is [ISC licensed](./LICENCE).
