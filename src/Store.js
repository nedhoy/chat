/**
 * Simple version of the Redux store.
 * We'll update to that later on.
 */

class Store {
	constructor(reducer) {
		this._reducer = reducer;
		this._listeners = [];

		/* Initial state */
		this._state = reducer(undefined, {});
	}

	getState() {
		return this._state;
	}

	/**
	 * Reduce the state based on the given action
	 *
	 * Any registers listeners are called.
	 */
	dispatch(action) {
		if (process.env.NODE_ENV !== "production") {
			console.log("action", action);
		}

		/* Note: Redux generalises this using middleware */
		if (typeof action === 'function') {
			return action(this.dispatch.bind(this), this.getState.bind(this));
		}
		else {
			this._state = this._reducer(this._state, action);
			this._listeners.forEach(listener => listener())

			if (process.env.NODE_ENV !== "production") {
				console.log("state", this._state);
			}
		}
	}

	/**
	 * Register listener
	 *
	 * The listener will be called whenever an action is dispatched.
	 *
	 * Returns a function that can be called to unsubscribe.
	 */
	subscribe(listener) {
		this._listeners.push(listener);

		return () => {
			const i = this._listeners.indexOf(listener);
			if (i >= 0) this._listeners.splice(i, 1);
		};
	}
}

export default Store;
