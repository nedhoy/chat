/*
 * @flow
 */

import { keyOf, compareCaseInsensitive } from "../utils.jsx"

import * as ActionType from "../constants/ActionType.js"

import {
	  ConnectionStatus
	, ThreadStatus
	, IrcStatus
	, ThreadType
} from "../types.js";

import type {
	  Action
	, TConnectionStatus
	, TIrcStatus
	, TThreadType
	, TThreadStatus
	, ThreadId
	, UserId
	, MessageId
	, Message
	, Dispatch
	, GetState
	, Dispatcher
} from "../types.js";

import type { State } from "../reduce.js";

import {
	  getThreadIdByChannelName
	, getThreadIdByPmUserId
	, getThreadByChannelName
	, getThreadByPmUserId
} from "../reducers/Thread.js";

/*
 * action creators
 */

export const setFormVisibility = (formId: number, show: boolean): Action =>
	({ type: ActionType.FORM_SHOW, formId, show })

export const submitWelcome = (): Action =>
	({ type: ActionType.SUBMIT_WELCOME })

export const setConnectionStatus = (connectionStatus: TConnectionStatus): Action =>
	({ type: ActionType.SET_CONNECTION_STATUS, connectionStatus })

export const setIrcStatus = (ircStatus: TIrcStatus): Action =>
	({ type: ActionType.SET_IRC_STATUS, ircStatus })

const setNickname = (nickname: string): Action =>
	({ type: ActionType.SET_NICKNAME, nickname })

const selectThread = (threadId: ThreadId): Action =>
	({ type: ActionType.THREAD_SELECT, threadId })

export const setThreadStatus = (threadId: ThreadId, threadStatus: TThreadStatus): Action =>
	({
		type: ActionType.THREAD_SET_STATUS,
		threadId,
		threadStatus,
	})

export const createChannel = (threadId: ThreadId, channelName: string): Action => {
	return ({
		type: ActionType.CHANNEL_CREATE,
		threadStatus: ThreadStatus.JOINED,
		channelName,
	});
}

export const createPm = (threadId: ThreadId, userId: UserId): Action => {
	return ({
		type: ActionType.PM_CREATE,
		threadStatus: ThreadStatus.JOINED,
		userId,
	});
}

const setChannelTopic = (channelName: string, topic: string): Action => {
	return ({
		type: ActionType.CHANNEL_SET_TOPIC,
		channelName,
		topic,
	});
}

const removeUserFromChannel = (threadId: ThreadId, userId: UserId): Action => {
	return ({
		type: ActionType.CHANNEL_REMOVE_USER,
		threadId,
		userId,
	});
}

const addUserToChannel = (threadId: ThreadId, userId: UserId): Action => {
	return ({
		type: ActionType.CHANNEL_ADD_USER,
		threadId,
		userId,
	});
}

const addUser = (userId: UserId, user: string): Action => {
	return ({
		type: ActionType.USER_ADD,
		userId,
		user,
	});
}

const addMessageToThread = (threadId: ThreadId, messageId: MessageId): Action => {
	return ({
		type: ActionType.THREAD_ADD_MESSAGE,
		threadId: threadId,
		messageId,
	});
}

const addMessage = (messageId: MessageId, message: Message): Action => {
	return ({
		type: ActionType.MESSAGE_ADD,
		messageId,
		message,
	});
}

const markMessageSeen = (messageId: MessageId): Action => {
	return ({
		type: ActionType.MESSAGE_MARK_SEEN,
		messageIds: [messageId],
	});
}

const markManyMessagesSeen = (messageIds: Array<MessageId>): Action => {
	return ({
		type: ActionType.MESSAGE_MARK_SEEN,
		messageIds,
	});
}

/*
 * dispatchers
 */

let nextThreadId: ThreadId = 0;

export const dispatchAddChannel = (name: string): Dispatcher<ThreadId> => {
	return (dispatch, getState) => {
		const state = getState();
		const threadId = getThreadIdFromChannelName(state, name);
		if (threadId != null) {
			return threadId;
		}
		dispatch(createChannel(nextThreadId, name));
		return nextThreadId++;
	}

}

export const dispatchAddPm = (userId: UserId): Dispatcher<ThreadId> => {
	return (dispatch, getState) => {
		const state = getState();
		const threadId = getThreadIdFromPmUserId(state, userId);
		if (threadId != null) {
			return threadId;
		}
		dispatch(createPm(nextThreadId, userId));
		return nextThreadId++;
	}
}

let nextUserId: UserId = 0;
export const dispatchAddUser = (user: string): Dispatcher<ThreadId> => {
	return (dispatch, getState) => {
		const state = getState();
		// Avoids adding duplicate users
		const userId = getIdForUser(state, user);
		if (userId != -1) {
			return userId;
		}

		dispatch(addUser(nextUserId, user));
		return nextUserId++;
	}
}

export const dispatchSelectThread = (threadId: ThreadId): Dispatcher<void> => {
	return (dispatch, getState) => {
		const state = getState();
		const thread = state.threads.threadsById[threadId];
		if (thread != null) {
			const messages = thread.messages;
			dispatch(markManyMessagesSeen(messages));
		}
		dispatch(selectThread(threadId));
	}
}

import parse from "../irc/parser.js";
import { Command, Numeric, makeCommand, makeNumeric } from "../irc/command.js";

const _getThreadNameFromPrivMsg = (state, command) => {
	if (command.type !== Command.PRIVMSG) {
		throw new TypeError("Expected " + Command.PRIVMSG);
	}

	const {author, target, text} = command;

	if (target.startsWith("#")) {
		/* Channel */
		return target;
	}
	else if (author === getCurrentNick(state)) {
		/* Dummy outgoing pm */
		return target;
	}
	else {
		/* Incoming PM */
		return author;
	}
}

export const dispatchAddMessageToThread = (threadId: ThreadId, messageId: MessageId): Dispatcher<void> => {
	return (dispatch, getState) => {
		const state = getState();
		const selectedThreadId = getSelectedThreadId(state);
		// FIXME: kind of a hack, we should probably determine
		//        whether we want the message seen before we
		//        add the message.
		if (selectedThreadId === threadId) {
			dispatch(markMessageSeen(messageId));
		}
		dispatch(addMessageToThread(threadId, messageId));
	}
}

let nextMessageId: MessageId = 0;
export const dispatchAddMessage = (message: Message): Dispatcher<MessageId> => {
	return dispatch => {
		message.seen = false;
		message.messageId = nextMessageId;
		dispatch(addMessage(nextMessageId, message));

		return nextMessageId++;
	}
}

const handleCommand = (state, dispatch, command) => {
	const messageId = dispatchAddMessage(command)(dispatch);

	switch (command.type) {
		case Command.JOIN: {
			const { nick, channels } = command;

			channels.forEach(channel => {
				// note that createChannel is idempotent, so it doesn't
				// matter if we have already added the channel.
				const threadId = dispatch(dispatchAddChannel(channel));
				// Ensure a thread is selected if any exist
				if (state.threads.selectedThread === null) {
					dispatch(dispatchSelectThread(threadId));
				}

				dispatch(setThreadStatus(threadId, ThreadStatus.JOINED));

				const userId = dispatch(dispatchAddUser(nick));
				dispatch(addUserToChannel(threadId, userId));
				dispatch(dispatchAddMessageToThread(threadId, messageId));
			});
			break;
		}
		case Command.PART: {
			const { nick, channels } = command;

			channels.forEach(channel => {
				const userId = getIdForUser(state, nick);
				if (userId != -1) {
					const threadId = getThreadIdFromChannelName(state, channel);
					if (threadId != null) {
						dispatch(removeUserFromChannel(threadId, userId));
						dispatch(dispatchAddMessageToThread(threadId, messageId));
					}
				}

				const thisUserId = getCurrentUserId(state);
				if (userId == thisUserId) {
					/* Close channel */
					const threadId = getThreadIdFromChannelName(state, channel);
					if (threadId != null) {
						dispatch(setThreadStatus(threadId, ThreadStatus.PARTED));
					}
				}
			});
			break;
		}
		case Command.QUIT: {
			const { nick, message } = command;

			const userId = getIdForUser(state, nick);
			if (userId === -1) {
				/* Unknown user */
				console.log(`Unknown user '${nick}' has QUIT`);
				break;
			}

			const channels = getChannels(state).filter(threadId => {
				const users = getThreadUsers(state, threadId);
				return users != null && users.indexOf(userId) >= 0;
			});
			channels.forEach(channel => {
				dispatch(removeUserFromChannel(channel, userId));
				dispatch(dispatchAddMessageToThread(channel, messageId));
			});

			// TODO: mark user as QUIT, useful for showing offline status in PMs

			const thisUserId = getCurrentUserId(state);
			if (userId === thisUserId) {
				// TODO: update connection status as quit
			}
		}
		case Command.PRIVMSG:
			const threadName = _getThreadNameFromPrivMsg(state, command);

			if (threadName.startsWith('#')) {
				const channel = threadName;
				const threadId = dispatch(dispatchAddChannel(channel));
				dispatch(dispatchAddMessageToThread(threadId, messageId));
			}
			else {
				const nick = threadName;
				const userId = dispatch(dispatchAddUser(nick)); // adds the user or retrieves user id if exists
				const threadId = dispatch(dispatchAddPm(userId)); // adds the thread or retrieves thread id
				dispatch(dispatchAddMessageToThread(threadId, messageId));
				dispatch(setThreadStatus(threadId, ThreadStatus.JOINED)); /* Show PM */
			}

			break;
		case Command.TOPIC:
			const { channel, topic } = command;

			// FIXME: need to define a MessageListItem render for TOPIC
			dispatch(setChannelTopic(channel, topic));
			break;
		case Command.MODE: {
			const { sender, nick } = command;

			// RFC2812 says MODE is only valid if the sender and
			// the nick are the same.
			if (sender === nick) {
				dispatch(setNickname(nick));
				//FIXME: set connection status to OK
			}
			break;
		}
		case Command.NICK: {
			// if we were clever, we might just store our nick with
			// id 0 and then we would not have to do anything special.
			const { old, nick } = command;
			if (old === getCurrentNick(state)) {
				dispatch(setNickname(nick));
				//TODO: set connection status to OK
			}
			const userId = getIdForUser(state, old);
			if (userId == -1) {
				console.log("NICK: unknown user");
				return;
			}
			// FIXME: 'addUser' also sets the user, not very semantic
			dispatch(addUser(userId, nick));

			break;
		}
		case Command.PING:
			// TODO
			break;
		case Command.ERROR:
			// TODO
			break;
	}
}

const handleNumeric = (state, dispatch, reply) => {
	switch (reply.type) {
		case Numeric.RPL_TOPIC:
			dispatch(setChannelTopic(reply.channel, reply.topic));
			break;
		case Numeric.RPL_NAME_REPLY:
			const channel = reply.channel;
			// FIXME: this message could have hundreds of names to add, so we should
			//        really add all these at once in a single action.
			reply.nicks.forEach(({ mode, nick }) => {
				// TODO: store mode
				const userId = dispatch(dispatchAddUser(nick));
				const threadId = getThreadIdFromChannelName(state, channel);
				if (threadId != null) {
					dispatch(addUserToChannel(threadId, userId));
				}
			});
			break;
		case Numeric.RPL_NICKNAME_IN_USE:
			// TODO: handle nickname in use by setting connection status and trying a new nick
			break;
	}
}

export const dispatchMessage = (message: Message): Dispatcher<void> => {
	return (dispatch, getState) => {
		const state = getState();
		const m = parse(message);

		const command = makeCommand(m);
		if (command) {
			handleCommand(state, dispatch, command);
		}

		const reply = makeNumeric(m);
		if (reply) {
			handleNumeric(state, dispatch, reply);
		}
	}
}

// NOTE that this does not actually send anything over the network
const sendPrivMsg = (target, text) => {
	return (dispatch, getState) => {
		const state = getState();
		const nick = getCurrentNick(state);
		const message = `:${nick} PRIVMSG ${target} :${text}`;
		dispatch(dispatchMessage(message));
	}
}

// NOTE that this does not actually send anything over the network
export const sendToSelectedChannel = (text: string): Dispatcher<void> => {
	return (dispatch, getState) => {
		const state = getState();
		const target = getSelectedThread(state);
		dispatch(sendPrivMsg(target, text));
	}
}


/*
 * getters
 */

export const getConnectionStatus = (state: State): TConnectionStatus => state.connected.connection_status;

export const getIrcStatus = (state: State): TIrcStatus => state.connected.irc_status;

export const getFormVisibility = (state: State, formId: number): boolean => {
	if (formId in state.forms) {
		return state.forms[formId];
	}

	/* If not found */
	return false;
}

export const getCurrentNick = (state: State): string => state.nickname;

export const getCurrentUserId = (state: State): UserId => {
	const nick = getCurrentNick(state);
	// FIXME: make sure the nickname always has an associated ID, currently
	//        that's not the case.
	const userId = getIdForUser(state, nick);

	return userId;
}

export const getMessagesForThread = (state: State, threadId: ThreadId): Array<Message> => {
	const thread = state.threads.threadsById[threadId];

	if (thread != null) {
		return thread.messages.map(messageId => state.messages[messageId]);
	}

	return [];
}

export const getMessagesForSelectedThread = (state: State): Array<Message> => {
	const threadId = state.threads.selectedThread;
	if (threadId != null) {
		return getMessagesForThread(state, threadId);
	}

	return [];
}

export const getThreadNameFromId = (state: State, threadId: ThreadId): string => {
	const thread = state.threads.threadsById[threadId];
	
	if (thread != null) {
		if (thread.info.type === "CHANNEL") {
			const channel = thread.info;
			return channel.name;
		}
		else if(thread.info.type === "PM") {
			const pm = thread.info;
			const user = getUserFromId(state, pm.user);
			return user;
		}
	}

	return "";
}

export const getThreadIdFromChannelName = (state: State, threadName: string): ?ThreadId => {
	return getThreadIdByChannelName(state.threads.threadsById, threadName);
}

export const getThreadIdFromPmUserId = (state: State, userId: UserId): ?ThreadId => {
	return getThreadIdByPmUserId(state.threads.threadsById, userId);
}

export const getSelectedThread = (state: State): string => {
	const threadId = parseInt(getSelectedThreadId(state));
	return getThreadNameFromId(state, threadId);
}

export const getSelectedThreadId = (state: State): ?ThreadId => {
	const threadId = state.threads.selectedThread;
	if (threadId == null) {
		return null;
	}

	return parseInt(threadId);
}

export const getThreadStatus = (state: State, threadId: ThreadId): TThreadStatus => {
	const thread = state.threads.threadsById[threadId];
	return thread.status;
}

export const isChannel = (state: State, threadId: ThreadId): boolean => {
	const thread = state.threads.threadsById[threadId];
	if (thread != null) {
		return thread.info.type === "CHANNEL";
	}

	return false;
}

export const getChannels = (state: State): Array<ThreadId> => {
	const threadIds = [];
	const threads = state.threads.threadsById;
	for (const k in threads) {
		const threadId = parseInt(k);
		const thread = threads[threadId];
		if (thread.info.type === "CHANNEL") {
			threadIds.push(threadId);
		}
	}
	return threadIds;
}

export const getChannelsWithStatus = (state: State, threadStatus: TThreadStatus): Array<ThreadId> => {
	const ids = getChannels(state);
	return ids.filter(id => getThreadStatus(state, id) === threadStatus);
}

export const getPrivateConversations = (state: State): Array<ThreadId> => {
	const threadIds = [];
	const threads = state.threads.threadsById;
	for (const k in threads) {
		const threadId = parseInt(k)
		const thread = threads[threadId];
		if (thread.info.type === "PM") {
			threadIds.push(threadId);
		}
	}
	return threadIds;
}

export const getPrivateConversationsWithStatus = (state: State, threadStatus: TThreadStatus): Array<ThreadId> => {
	const ids = getPrivateConversations(state);
	return ids.filter(id => getThreadStatus(state, id) === threadStatus);
}

export const getChannelTopic = (state: State, channel: string): ?string => {
	const thread = getThreadByChannelName(state.threads.threadsById, channel);
	if (thread != null && thread.info.type === "CHANNEL") {
		const channel = thread.info;
		return channel.topic;
	}

	return null;
}

export const getChannelUserIds = (state: State, channel: string): ?Array<UserId> => {
	const thread = getThreadByChannelName(state.threads.threadsById, channel);
	if (thread != null && thread.info.type === "CHANNEL") {
		const channel = thread.info;
		return channel.users;
	}

	return null;
}

export const getThreadUsers = (state: State, threadId: ThreadId): ?Array<UserId> => {
	const thread = state.threads.threadsById[threadId];
	if (thread != null) {
		if (thread.info.type === "CHANNEL") {
			const channel = thread.info;
			return channel.users;
		}
		else if (thread.info.type === "PM") {
			const pm = thread.info;
			return [pm.user];
		}
	}

	return null;
}

// FIXME: better to return [{id, nick}] than just [id]
export const getChannelUserIdsSorted = (state: State, channel: string): ?Array<UserId> => {
	// FIXME: it may be more efficient to convert all the names to uppercase first,
	//        so that the comparison function isn't repeatedly transforming the same
	//        strings over and over again. The MDN page on `sort` has a good treatment
	//        of this.
	const userIds = getChannelUserIds(state, channel);
	if (userIds != null) {
		const nicks = userIds.map(id => ({id, nick: getUserFromId(state, id)}));
		nicks.sort(({ nick: a }, { nick: b }) => compareCaseInsensitive(a, b));
		return nicks.map(({ id }) => id);
	}

	return undefined;
}

export const getUserFromId = (state: State, userId: UserId): string => {
	return state.users[userId];
}

const getIdForUser = (state: State, user: string): UserId => {
	for (const k in state.users) {
		const id = parseInt(k);
		if (state.users[id] === user) {
			// Convert back to a number because we're storing the mapping as a record
			return id;
		}
	}

	return -1;
}

export const getUnseenCountForThread = (state: State, thread: ThreadId): number => {
	const messages = getMessagesForThread(state, thread);
	const sum = (xs) => xs.reduce((n, _) => n + 1, 0);
	const filter = (m) => !m.seen && m.type === Command.PRIVMSG;
	const count = sum(messages.filter(filter));

	return count;

}
