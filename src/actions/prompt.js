/*
 * @flow
 */

import * as ActionType from "../constants/ActionType.js"

import type {
	  Action
	, Dispatcher
	, ThreadId
} from "../types.js";
import type { State } from "../reduce.js";
/*
 * action creators
 */

export const setPromptValue = (threadId: ThreadId, value: string): Action =>
	({ type: ActionType.PROMPT_SET_VALUE, threadId, value })

export const newPromptHistoryItem = (threadId: ThreadId): Action =>
	({ type: ActionType.PROMPT_HISTORY_NEW, threadId })

export const historyUp = (threadId: ThreadId): Action =>
	({ type: ActionType.PROMPT_HISTORY_UP, threadId })

export const historyDown = (threadId: ThreadId): Action =>
	({ type: ActionType.PROMPT_HISTORY_DOWN, threadId })

export const setAutocompleteList = (threadId: ThreadId, completions: Array<string>): Action =>
	({ type: ActionType.PROMPT_AUTOCOMPLETE_SET_LIST, threadId, completions })

export const autocomplete = (threadId: ThreadId): Action =>
	({ type: ActionType.PROMPT_AUTOCOMPLETE_CYCLE, threadId })

/*
 * dispatchers
 */

export const dispatchSetAutocompleteList = (threadId: ThreadId, completions: Array<string>): Dispatcher<void> => {
	return (dispatch, getState) => {
		const state = getState();
		const composer = getComposer(state, threadId);

		/*
		 * For efficiency, only set completion list when
		 * there is no current completion.
		 */
		if (composer.completions.length == 0) {
			const value = getComposerValue(composer);
			const fragments = value.split(' ');
			const fragment = fragments[fragments.length - 1];
			const matches = completions.filter(s => s.startsWith(fragment));

			/*
			 * No point setting completion list if there are no matches
			 */
			if (matches.length > 0) {
				dispatch(setAutocompleteList(threadId, matches));
			}
		}
	}
}


/*
 * getters
 */

const getComposer = (state, threadId) => state.composer[threadId];

const getComposerValue = (composer) => {
	if (composer) {
		const { stack, index } = composer;
		return stack[index];
	}

	return "";
}

export const getCompletedValue = (state: State, threadId: ThreadId): string => {
	const composer = getComposer(state, threadId);
	if (composer) {
		const { completions, completion_index } = composer;

		const value = getComposerValue(composer);
		if (completions.length == 0) {
			return value;
		}

		// The completion is the current value, with the last fragment
		// replaced by the completion.
		const fragments = value.split(' ');
		const fragment = fragments[fragments.length - 1];

		const completion = completions[completion_index];
		const result = fragments.slice(0, -1).concat(completion).join(' ');

		return result;
	}

	return "";
}

export const getValue = (state: State, threadId: ThreadId): string => {
	// FIXME: probably need to calculate some stuff here
	const composer = getComposer(state, threadId);
	// return composer ? composer.value : "";
	if (composer) {
		const { stack, index } = composer;
		return stack[index];
	}

	return "";
}
