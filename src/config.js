const config = {
	/* Network */
	host:        "webirc.oftc.net",
	port:        "8443",

	persistence: {
		host:    "https://chat.example.org",
		network: "OFTC",
	},

	/* User */
	nick:        "me_testing",
	user:        "me",
	real:        "me",

	/* Default Channels */
	channels:    ["#testing1234", "#testing1234567"],

	/**
	 * If `askChannels` is true then the welcome page will allow
	 * the user to specify which channels to join. Otherwise the
	 * form field will be hidden.
	 */
	askChannels: true,
}

export default config;
