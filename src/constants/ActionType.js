/*
 * @flow
 */

/*
 * action types
 */

export const SUBMIT_WELCOME        = "SUBMIT_WELCOME";
export const SET_CONNECTION_STATUS = "SET_CONNECTION_STATUS";
export const SET_IRC_STATUS        = "SET_IRC_STATUS";

export const CHANNEL_CREATE        = "CHANNEL_CREATE";
export const CHANNEL_SET_TOPIC     = "CHANNEL_SET_TOPIC";
export const CHANNEL_ADD_USER      = "CHANNEL_ADD_USER";
export const CHANNEL_REMOVE_USER   = "CHANNEL_REMOVE_USER";

export const PM_CREATE             = "PM_CREATE";

export const MESSAGE_ADD           = "MESSAGE_ADD";
export const MESSAGE_MARK_SEEN     = "MESSAGE_MARK_SEEN";
export const USER_ADD              = "USER_ADD";
export const SET_NICKNAME          = "SET_NICKNAME";

export const THREAD_SELECT         = "THREAD_SELECT";
export const THREAD_ADD_MESSAGE    = "THREAD_ADD_MESSAGE";
export const THREAD_SET_STATUS     = "THREAD_SET_STATUS";

export const FORM_SHOW             = "FORM_SHOW";

export const PROMPT_SET_VALUE    = "PROMPT_SET_VALUE"
export const PROMPT_HISTORY_NEW  = "PROMPT_HISTORY_NEW"
export const PROMPT_HISTORY_UP   = "PROMPT_HISTORY_UP"
export const PROMPT_HISTORY_DOWN = "PROMPT_HISTORY_DOWN"

export const PROMPT_AUTOCOMPLETE_SET_LIST = "PROMPT_AUTOCOMPLETE_SET_LIST"
export const PROMPT_AUTOCOMPLETE_CYCLE    = "PROMPT_AUTOCOMPLETE_CYCLE"
