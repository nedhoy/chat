import "babel-polyfill";
import "whatwg-fetch"; /* polyfill */

import React from "react";
import ReactDOM from "react-dom";

import App from "./ui/App.jsx";
import Store from "./Store.js";
import reduce from "./reduce.js";

const store = new Store(reduce);

ReactDOM.render(
	<App store={store} />,
	document.getElementById('irc-app')
);
