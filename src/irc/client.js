/*
 * @flow
 */

import type { socket } from "socket.io-client";

import parse from "./parser.js";
import { Command, Numeric, makeCommand, makeNumeric } from "./command.js";
import type { TCommand, TNumeric } from "./command.js";
import ConnectionState from "./connection.js";
import type { TConnectionState } from "./connection.js";

type TClientEvent = "REGISTERED" | "ERROR"

export const ClientEvent = {
	REGISTERED: "REGISTERED",
	ERROR:      "ERROR",
}

type Callback = () => void;

/**
 * IRC client.
 *
 * Provides methods for sending IRC commands.
 */
export default class Client {
	socket:       socket;
	_state:       TConnectionState;
	_queue:       Array<string>;
	_nickname:    string;
	_handlers:    {[key: TClientEvent]: Callback};
	_pendingNick: string;

	/**
	 * Takes a socket.io socket.
	 */
	constructor(socket: socket, initialNick: string) {
		this.socket = socket;
		this._state = ConnectionState.New;
		this._queue = [];
		this._nickname = initialNick;
		this._handlers = {};
	}

	/**
	 * Set the callback handler for the specified event.
	 */
	on(event: TClientEvent, callback: Callback) {
		this._handlers[event] = callback;
	}

	_setState(state: TConnectionState, ...args: Array<string>) {
		console.log(`ConnectionState ${this._state.toString()} -> ${state.toString()}`);

		this._state = state;

		switch (state) {
			case ConnectionState.Error:
				const callback = this._handlers[ClientEvent.ERROR];
				if (callback !== undefined) {
					callback(...args);
				}
				break;
			case ConnectionState.Ok:
				this._flush_queue();
				break;
		}
	}

	_send(s: string) {
		const msg = s + "\r\n";
		this.socket.send(msg);

		console.log("--> " + msg);
	}

	_enqueue(s: string) {
		this._queue.push(s);
	}

	_flush_queue() {
		this._queue.forEach(s => this._send(s));
		this._queue = [];
	}

	privmsg(target: string, text: string) {
		const command = `PRIVMSG ${target} :${text}`;
		this._send(command);

		// FIXME: hack.
		// IRC doesn't echo PRIVMSG commands, so we artificially
		// insert the outgoing message into our history.
		this.handleMessage(`:${this.getNick()} ${command}`);
	}

	nick(nick: string) {
		this._send(`NICK ${nick}`);
		this._pendingNick = nick;
	}
	
	user(user: string, real: string) {
		this._send(`USER ${user} * * :${real}`);
	}

	join(channels: Array<string>) {
		this._enqueue(`JOIN ${channels.join(',')}`);
		/**
		 * During the connection phase we want to change our
		 * nick and then join the channels. We could send a JOIN
		 * command immediately following the NICK command, however
		 * if the NICK command is rejected then we join the channel
		 * before we have a chance to try changing the nick again.
		 *
		 * Instead, we wait for the requested nick to be confirmed before
		 * we join the channel.
		 */
		if (this._pendingNick !== this._nickname) {
			console.log("Waiting for nick to stabilise before joining channel");
		}
		else if (this._state === ConnectionState.Ok) {
			this._flush_queue();
		}
	}

	part(channels: Array<string>) {
		this._send(`PART ${channels.join(',')}`);
	}

	quit(message: string) {
		this._send(`QUIT :${message}`);
	}

	getNick(): string {
		return this._nickname;
	}

	handleMessage(message: string) {
		const m = parse(message);

		const command = makeCommand(m);
		if (command) {
			const date = new Date();
			command.date = date;

			this._handleCommand(command);
		}

		const reply = makeNumeric(m);
		if (reply) {
			this._handleNumeric(reply);
		}
	}

	_handleCommand(command: TCommand) {
		const dispatcher = {};
		dispatcher[Command.MODE]    = (c) => this._handleMode(c);
		dispatcher[Command.NICK]    = (c) => this._handleNick(c);
		dispatcher[Command.PING]    = (c) => this._handlePing(c);
		dispatcher[Command.ERROR]   = (c) => this._handleError(c);

		const handler = dispatcher[command.type]
		if (handler !== undefined) {
			handler(command);
		}
	}

	_handleNumeric(reply: TNumeric) {
		switch (reply.type) {
			case Numeric.ERR_NICKNAME_IN_USE:
				this._handleRplNicknameInUse(reply);
				break;
		}
	}

	_handleMode(command: TCommand) {
		if (command.type !== Command.MODE) {
			throw new TypeError("Expected Command.MODE");
		}

		// RFC2812 says MODE is only valid if the sender and
		// the nick are the same.
		if (command.sender === command.nick) {
			this._nickname = command.nick;
			this._setState(ConnectionState.Ok);

			const callback = this._handlers[ClientEvent.REGISTERED];
			if (callback !== undefined) {
				callback();
			}
		}

		// TODO: store MODE flags
	}

	_handleNick(command: TCommand) {
		if (command.type !== Command.NICK) {
			throw new TypeError("Expected Command.NICK");
		}

		this._nickname = command.nick;

		this._setState(ConnectionState.Ok);
	}

	_handlePing(command: TCommand) {
		if (command.type !== Command.PING) {
			throw new TypeError("Expected Command.PING");
		}

		this._send(`PONG ${command.server}`);
	}

	_handleError(command: TCommand) {
		if (command.type !== Command.ERROR) {
			throw new TypeError("Expected Command.ERROR");
		}

		this._setState(ConnectionState.Error, command.error);
	}

	_handleRplNicknameInUse(reply: TNumeric) {
		if (reply.type !== Numeric.ERR_NICKNAME_IN_USE) {
			throw new TypeError("Expected Numeric.ERR_NICKNAME_IN_USE");
		}

		this._setState(ConnectionState.NicknameInUse);

		const { new_nick } = reply;

		const similarify = (name) => name + "_";
		this.nick(similarify(new_nick));
	}
}
