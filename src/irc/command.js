/*
 * @flow
 */

import parse from "./parser.js";

import type { Prefix, RawCommand } from "./parser.js";

export const Command = {
	JOIN:    "JOIN",
	PART:    "PART",
	QUIT:    "QUIT",
	PRIVMSG: "PRIVMSG",
	TOPIC:   "TOPIC",
	MODE:    "MODE",
	NICK:    "NICK",
	PING:    "PING",
	ERROR:   "ERROR",
};

export type TCommand =
	| Join
	| Part
	| Quit
	| PrivMsg
	| Topic
	| Mode
	| Nick
	| Ping
	| Error

type Join = {
	  type     : "JOIN"
	, date     : Date
	, prefix   : Prefix
	, nick     : string
	, channels : Array<string>
}

type Part = {
	  type     : "PART"
	, date     : Date
	, prefix   : Prefix
	, nick     : string
	, channels : Array<string>
}

type Quit = {
	  type    : "QUIT"
	, date    : Date
	, prefix  : Prefix
	, nick    : string
	, message : string
}

type PrivMsg = {
	  type   : "PRIVMSG"
	, date   : Date
	, author : string
	, target : string
	, text   : string
}

type Topic = {
	  type    : "TOPIC"
	, date    : Date
	, channel : string
	, topic   : string
}

type Mode = {
	  type   : "MODE"
	, date   : Date
	, sender : string
	, nick   : string
	, flags  : string
}

type Nick = {
	  type : "NICK"
	, date : Date
	, old  : string
	, nick : string
}

type Ping = {
	  type   : "PING"
	, date   : Date
	, server : string
}

type Error = {
	  type  : "ERROR"
	, date  : Date
	, error : string
}

export const Numeric = {
	RPL_TOPIC:           "RPL_TOPIC",
	RPL_NAME_REPLY:      "RPL_NAME_REPLY",
	RPL_END_OF_NAMES:    "RPL_END_OF_NAMES",
	ERR_NICKNAME_IN_USE: "RPL_NICKNAME_IN_USE",
};

export type TNumeric =
	| RplTopic
	| RplNameReply
	| RplEndOfNames
	| ErrNicknameInUse

type RplTopic = {
	  type    : "RPL_TOPIC" /* 332 */
	, channel : string
	, topic   : string
}

type RplNameReply = {
	  type    : "RPL_NAME_REPLY" /* 353 */
	, channel : string
	, nicks   : Array<{ mode: string, nick: string }>
}

type RplEndOfNames = {
	  type    : "RPL_END_OF_NAMES" /* 366 */
	, channel : string
}

type ErrNicknameInUse = {
	  type     : "RPL_NICKNAME_IN_USE" /* 433 */
	, old_nick : string
	, new_nick : string
}

export function makeCommand(rawCommand: RawCommand): ?TCommand {
	const { prefix, command, args } = rawCommand;
	const date = new Date();
	switch(command) {
		case "JOIN":
			return {
				type:     Command.JOIN,
				date,
				prefix:   prefix,
				nick:     prefix.name,
				channels: args,
			}
		case "PART":
			return {
				type:     Command.PART,
				date,
				prefix:   prefix,
				nick:     prefix.name,
				channels: args,
			}
		case "QUIT":
			return {
				type:    Command.QUIT,
				date,
				prefix:  prefix,
				nick:    prefix.name,
				message: args[0],
			}
		case "PRIVMSG":
			const nick = prefix.name;
			return {
				type:   Command.PRIVMSG,
				date,
				author: nick,
				target: args[0],
				text:   args[1],
			}
		case "TOPIC":
			return {
				type:    Command.TOPIC,
				date,
				channel: args[0],
				topic:   args[1],
			};
		case "MODE":
			// TODO: parse flags
			return {
				type:   Command.MODE,
				date,
				sender: prefix.name,
				nick:   args[0],
				flags:  args[1],
			};
		case "NICK":
			return {
				type: Command.NICK,
				date,
				old:  prefix.name,
				nick: args[0],
			};
		case "PING":
			return {
				type:   Command.PING,
				date,
				server: args[0],
			};
		case "ERROR":
			return {
				type:  Command.ERROR,
				date,
				error: args[0],
			};
	}

	return null;
}

export function makeNumeric(rawCommand: RawCommand): ?TNumeric {
	const {prefix, command, args} = rawCommand;
	const code = getCode(command);

	// FIXME: Not in RFC 2821, but OFTC seems to insert our
	//        nick as the first argument, so we ignore args[0].
	switch (code) {
		case Numeric.RPL_TOPIC:
			return {
				type:    Numeric.RPL_TOPIC,
				channel: args[1],
				topic:   args[2],
			};
		case Numeric.RPL_NAME_REPLY:
			let i = 1;
			if ("=*@".includes(args[1])) {
				i++;
				// TODO store the visibility in the returned object
			}
			const channel = args[i++];
			const rawNicks = args[i++].split(' ');

			const nicks = rawNicks.map(s => {
				if ("@+".includes(s[0])) {
					const mode = s[0];
					const nick = s.substr(1);
					return { mode, nick };
				}

				const mode = ''; /* no mode */
				const nick = s;
				return { mode, nick };
			});

			return {
				type:    Numeric.RPL_NAME_REPLY,
				channel: channel,
				nicks:   nicks,
			};
		case Numeric.RPL_END_OF_NAMES:
			return {
				type:    Numeric.RPL_END_OF_NAMES,
				channel: args[1],
			};
		case Numeric.ERR_NICKNAME_IN_USE:
			return {
				type:     Numeric.ERR_NICKNAME_IN_USE,
				old_nick: args[0],
				new_nick: args[1],
			};
	}

	return null;
}

export function getCode(numeric: string): ?string {
	switch (numeric) {
		case "332": return Numeric.RPL_TOPIC;
		case "353": return Numeric.RPL_NAME_REPLY;
		case "366": return Numeric.RPL_END_OF_NAMES;
		case "433": return Numeric.ERR_NICKNAME_IN_USE;
	}

	return null;
}
