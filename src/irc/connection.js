export type TConnectionState =
	| "New"
	| "Ok"
	| "Nickname in use"
	| "Error"

const ConnectionState = {
	New          : Symbol("New"),
	Ok           : Symbol("Ok"),
	NicknameInUse: Symbol("Nickname in use"),
	Error        : Symbol("Error"),
};

export default ConnectionState;
