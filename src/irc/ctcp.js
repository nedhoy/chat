/*
 * @flow
 */

import { Command } from "./command.js"
import { splitOnce } from "./parser.js"

import type { TCommand } from "./command.js"

export const CTCPCommand = {
	ACTION: "ACTION",
}

export type TCTCPCommand =
	| Action

type Action = {
	  type   : "ACTION"
	, date   : Date
	, author : string
	, text   : string
}

export const makeCTCPCommand = (message: TCommand): ?TCTCPCommand => {
	if (message.type !== Command.PRIVMSG) {
		return null;
	}

	const text = message.text;

	if (!text.startsWith('\x01')) {
		return null;
	}

	/* Strip the \0x01 CTCP markers */
	const raw = text.replace(/^\x01|\x01$/g, '');

	const [command, args] = splitOnce(raw, ' ');

	switch (command) {
		case "ACTION":
			return {
				  type   : CTCPCommand.ACTION
				, text   : args
				, date   : message.date
				, author : message.author
			}
	}

	return null;
}
