/*
 * @flow
 */

export type Prefix = {
	  name: string
	, user: string
	, host: string
}

export type RawCommand = {
	  prefix  : Prefix
	, command : string
	, args    : Array<string>
}

// utility function
export function splitOnce(s: string, pattern: string): [string, string] {
	const i = s.indexOf(pattern);
	if (i >= 0) {
		return [s.slice(0, i), s.slice(i + 1)];
	}
	else {
		return [s, ""];
	}
}

/**
 * Parse IRC messages of the form
 *     [':' name ['!' nick ['@' host]] command args [longarg]
 */
export default function parse(s: string): RawCommand {
	let prefix = "";
	let remainder = s;
	if (s.startsWith(':')) {
		[prefix, remainder] = splitOnce(s.substr(1), ' ');
	}

	const [command, args] = splitOnce(remainder, ' ');

	return {
		prefix:  parsePrefix(prefix),
		command: command,
		args:    parseArgs(args),
	};
}

/**
 * IRC command arguments are separated by whitespace, however the last
 * argument is permitted to have whitespace if it is prefixed with a ':'.
 *
 * For example,
 *     "arg1 arg2 arg3 :this is arg4"
 * would yield ["arg1", "arg2", "arg3", ":this is arg4"].
 */
function parseArgs(s) {
	const args = s.split(' ');

	for (let i = 0; i < args.length; i++) {
		if (args[i].startsWith(':')) {
			const right = args.splice(i);
			right[0] = right[0].replace(':', '');

			const merged = right.join(' ');
			args[i] = merged;

			return args;
		}
	}

	return args;
}

/**
 * IRC prefixes are of the form
 *     servername
 * or
 *     nickname ['!' user ['@' host]]
 * 
 * However, it's not obvious how to disambiguate between the first form and
 * the case where the second form does not have the optional components.
 * This parser does not attempt to do so, instead we just return each
 * component and leave it to the caller to make that decision. If an optional
 * component is not present then an empty string is used.
 */
function parsePrefix(s: string): Prefix {
	const [name, remainder = ""] = s.split('!');
	const [user, host = ""] = remainder.split('@');

	return {name, user, host};
}
