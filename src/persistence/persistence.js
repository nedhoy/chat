/**
 * Wrapper around fetch that expects status code 200
 * otherwise throws an error.
 */
const fetchExpect200 = (url) => {
	return new Promise((resolve, reject) => {
		fetch(url)
			.then(response => {
				if (response.status !== 200) {
					console.log("Problem fetching from " + url);
					reject("Bad status code: " + response.status);
					return;
				}

				resolve(response);
			})
			.catch(e => reject(e))
	});
}

/**
 * Retrieves a list of networks and channels on those
 * networks that have history available.
 */
export const getNetworks = (host) => {
	const promise = new Promise((resolve, reject) => {
		const url = `${host}/listening`;
		fetchExpect200(url)
			.then(response => {
				resolve(response.json())
			})

			.catch(e =>	reject(e))
	});

	return promise;
}

/**
 * Retrieves the `n` most recent messages from the
 * given channel on the given network.
 */
export const getMessages = (host, network, channel, n) => {
	const promise = new Promise((resolve, reject) => {
		const encodedChannel = encodeURIComponent(channel);
		const url = `${host}/get/${network}/${encodedChannel}/${n}`;

		fetchExpect200(url)
			.then(response => {
				resolve(response.json())
			})

			.catch(e => reject(e));
	});

	return promise;
}

/**
 * Retrieves the `n` messages prior to the message with
 * the given id.
 */
export const getMessagesBefore = (host, network, channel, n, id) => {
	const promise = new Promise((resolve, reject) => {
		const encodedChannel = encodeURIComponent(channel);
		const url = `${host}/get/${network}/${encodedChannel}/${n}/${id}`;

		fetchExpect200(url)
			.then(response => {
				resolve(response.json())
			})

			.catch(e => reject(e));
	});

	return promise;
}

/**
 * Converts a string in the format
 *     Sat, 14 May 2016 13:29:19 GMT
 * to a Date object.
 */
const parseDate = (date) => {
	// FIXME: yielding incorrect date
	return new Date(date);
}

import { Command } from "../irc/command.js";

const convertToLocalFormat = (m) => {
	const { action } = m;
	switch (action) {
		case "PRIVMSG":
		{
			const { channel, msg, nick, time } = m;
			const date = parseDate(time);
			return {
				type:   Command.PRIVMSG,
				author: nick,
				target: channel,
				text:   msg,
				date:   date,
			};
		}
		case "JOIN":
		{
			const { channel, nick, time } = m;

			const date = parseDate(time);
			return {
				type:     Command.JOIN,
				// FIXME: prefix not provided by api, so fake one
				prefix:   { user: nick },
				nick:     nick,
				channels: [channel],
				date:     date,
			};
		}
		case "PART":
		{
			const { channel, nick, time } = m;

			const date = parseDate(time);
			return {
				type:     Command.PART,
				// FIXME: prefix not provided by api, so fake one
				prefix:   { user: nick },
				nick:     nick,
				channels: [channel],
				date:     date,
			};
		}
		case "QUIT":
		{
			const { nick, time, message } = m;

			const date = parseDate(time);
			return {
				type:     Command.QUIT,
				// FIXME: prefix not provided by api, so fake one
				prefix:   { user: nick },
				nick:     nick,
				date:     date,
				message:  message,
			};
		}
		default:
			return null;
	}
};

/**
 * Connect to `host` and fetch messages for all specified `channels` on the
 * given `network`.
 */
export const getMessagesFrom = (host, network, channels) => {
	const getChannels = network => networks => networks[network];

	const filterIn = xs => ys => ys.filter(y => xs.includes(y));

	const zipObject = keys => values => {
		if (keys.length != values.length) {
			return null; /* FIXME: or raise error */
		}

		const object = {};
		values.forEach((value, i) => {
			const key = keys[i];
			object[key] = value;
		});

		return object;
	};

	const getChannelMessages = (host, network) => channels => {
		const promises = channels.map(channel => {
			return getMessages(host, network, channel, 50)
				.then(ms => {
					const messages = Object.values(ms);
					return messages.map(convertToLocalFormat).filter(x => x != null);
				});
		});
		return Promise.all(promises).then(zipObject(channels));
	};

	return getNetworks(host)
		.then(getChannels(network))
		.then(filterIn(channels))
		.then(getChannelMessages(host, network))
}
