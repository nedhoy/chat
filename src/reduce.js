/*
 * @flow
 */

import welcome from "./reducers/Welcome.js"
import connected from "./reducers/Connection.js"
import threads from "./reducers/Thread.js"
import messages from "./reducers/Message.js"
import users from "./reducers/User.js"
import nickname from "./reducers/Nickname.js";
import composer from "./reducers/Composer.js";

import { FORM_SHOW } from "./constants/ActionType.js";

import { ConnectionStatus, IrcStatus } from  "./types.js";

import type { Action } from "./types.js";

import type { Threads } from "./reducers/Thread.js";
import type { ComposerMap } from "./reducers/Composer.js";
import type { Connection } from "./reducers/Connection.js";
import type { Welcome } from "./reducers/Welcome.js";
import type { Users } from "./reducers/User.js";
import type { Nickname } from "./reducers/Nickname.js";
import type { Messages } from "./reducers/Message.js";

import { initialState as initialThreadState } from "./reducers/Thread.js";

export type Forms    = {[key: number]: boolean};
const forms = (state = {}, action) => {
	switch (action.type) {
		case FORM_SHOW:
		{
			const { formId, show } = action;
			return {
				...state,
				[formId]: show,
			}
		}
		default:
			return state;
	}
}

export type State = {
	  welcome:   Welcome
	, connected: Connection
	, nickname:  Nickname
	, threads:   Threads
	, messages:  Messages
	, users:     Users
	, composer:  ComposerMap
	, forms:     Forms
};

const initialState = {
	  welcome:   {finished: false}
	, connected: {
		  connection_status: ConnectionStatus.INIT
		, irc_status:        IrcStatus.INIT
	}
	, nickname:  ""
	, threads:   initialThreadState
	, messages:  {}
	, users:     {}
	, composer:  {}
	, forms:     {}
};

export default function reduce(state: State = initialState, action: Action) {
	return {
		welcome:   welcome(state.welcome, action),
		connected: connected(state.connected, action),
		nickname:  nickname(state.nickname, action),
		threads:   threads(state.threads, action),
		messages:  messages(state.messages, action),
		users:     users(state.users, action),
		composer:  composer(state.composer, action),
		forms:     forms(state.forms, action),
	}
}
