/*
 * @flow
 */

import {
	PROMPT_SET_VALUE,
	PROMPT_HISTORY_NEW,
	PROMPT_HISTORY_UP,
	PROMPT_HISTORY_DOWN,
	PROMPT_AUTOCOMPLETE_SET_LIST,
	PROMPT_AUTOCOMPLETE_CYCLE,
} from "../constants/ActionType.js";

import type {
	  Action
	, ThreadId
} from "../types.js";

export type Composer = {
	  stack:            Array<string>
	, index:            number
	, completions:      Array<string>
	, completion_index: number
};

export type ComposerMap = {[key: ThreadId]: Composer};

const initialState: Composer = {
	  stack:            [""]
	, index:            0
	, completions:      []
	, completion_index: 0
};

export function thread(state: Composer = initialState, action: Action): Composer {
	const { stack, index } = state;
	switch (action.type) {
		case PROMPT_SET_VALUE:
		{
			const left = stack.slice(0, index);
			const right = stack.slice(index + 1);
			const newStack = [
				...left,
				action.value,
				...right
			]
			return {
				...state,
				stack: newStack,
				/* Reset autocomplete */
				completions: [],
				completion_index: 0,
			}
		}
		case PROMPT_HISTORY_NEW:
		{
			const newStack = [
				"",
				...stack,
			]
			return {
				...state,
				stack: newStack,
				index: 0,
				/* Reset autocomplete */
				completions: [],
				completion_index: 0,
			}
		}
		case PROMPT_HISTORY_UP:
			return {
				...state,
				index: Math.min(index + 1, stack.length - 1),
			}
		case PROMPT_HISTORY_DOWN:
			return {
				...state,
				index: Math.max(index - 1, 0),
			}
		case PROMPT_AUTOCOMPLETE_SET_LIST:
		{
			const { completions } = action;
			return {
				...state,
				completions: completions,
				completion_index: 0,
			}
		}
		case PROMPT_AUTOCOMPLETE_CYCLE:
		{
			const { completions, completion_index } = state;
			return {
				...state,
				completion_index: (completion_index + 1) % completions.length,
			}
		}
		default:
			return state;
	}
}

export default function composer(state: ComposerMap = {}, action: Action): ComposerMap {
	switch (action.type) {
		case PROMPT_SET_VALUE:
		case PROMPT_HISTORY_NEW:
		case PROMPT_HISTORY_UP:
		case PROMPT_HISTORY_DOWN:
		case PROMPT_AUTOCOMPLETE_SET_LIST:
		case PROMPT_AUTOCOMPLETE_CYCLE:
			const { threadId } = action;
			return {
				...state,
				[threadId]: thread(state[threadId], action),
			}
		default:
			return state;
	}
}
