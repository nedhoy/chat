/*
 * @flow
 */

import {
	SET_CONNECTION_STATUS,
	SET_IRC_STATUS,
} from "../constants/ActionType.js"

import {
	ConnectionStatus,
	IrcStatus,
} from "../types.js"

import type {
	  Action
	, TConnectionStatus
	, TIrcStatus
} from "../types.js";

export type Connection = {
	  connection_status: TConnectionStatus
	, irc_status:        TIrcStatus
};

const initialState: Connection = {
	  connection_status: ConnectionStatus.INIT
	, irc_status:        IrcStatus.INIT
};

export default function connected(state: Connection = initialState, action: Action): Connection {
	switch (action.type) {
		case SET_IRC_STATUS:
			return {
				...state,
				irc_status: action.ircStatus,
			}
		case SET_CONNECTION_STATUS:
			return {
				...state,
				connection_status: action.connectionStatus,
			}
		default:
			return state;
	}
}
