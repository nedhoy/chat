/*
 * @flow
 */

import {
	MESSAGE_ADD,
	MESSAGE_MARK_SEEN,
} from "../constants/ActionType.js"

import type {
	  Action
	, MessageId
	, Message
} from "../types.js";

export type Messages = {[key: MessageId]: Message};

export default function messages(state: Messages = {}, action: Action): Messages {
	switch (action.type) {
		case MESSAGE_ADD: {
			const { messageId, message } = action;
			return {
				...state,
				[messageId]: message,
			}
		}
		case MESSAGE_MARK_SEEN: {
			const { messageIds } = action;
			const newState = {};
			for (const k in state) {
				const mid = parseInt(k);
				const message = state[mid];
				// Have to convert to an int, because the hacky way
				// we store a map is in a record where the keys are
				// strings.
				if (messageIds.indexOf(mid) != -1) {
					newState[mid] = {...message, seen: true};
				}
				else {
					newState[mid] = {...message};
				}
			}
			return newState;
		}
		default:
			return state;
	}
}
