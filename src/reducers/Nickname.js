/*
 * @flow
 */

import { SET_NICKNAME } from "../constants/ActionType.js"

import type { Action } from "../types.js";

export type Nickname = string;

export default function nickname(state: Nickname = "", action: Action): Nickname {
	switch (action.type) {
		case SET_NICKNAME:
			return action.nickname;
		default:
			return state;
	}
}


