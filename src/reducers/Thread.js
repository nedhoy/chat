/*
 * @flow
 */

import {
	CHANNEL_CREATE,
	CHANNEL_SET_TOPIC,
	CHANNEL_ADD_USER,
	CHANNEL_REMOVE_USER,
	PM_CREATE,
	THREAD_ADD_MESSAGE,
	THREAD_SELECT,
	THREAD_SET_STATUS,
} from "../constants/ActionType.js"

import { ThreadStatus } from "../types.js";

import type {
	  Action
	, TThreadStatus
	, ThreadId
	, MessageId
	, UserId
} from "../types.js";


/* Record for a single thread */
export type Thread = {
	  threadId: ThreadId
	, messages: Array<MessageId>
	, status:   TThreadStatus

	/* Factored out channel and pm information */
	, info:     ThreadInfo
}

export type ThreadInfo = Channel | PrivateMessage;

export type Channel = {
	  type:     "CHANNEL"
	, users:    Array<UserId>
	, topic:    string
	, name:     string
};

export type PrivateMessage = {
	  type:     "PM"
	, user:     UserId
};

export type ThreadMap = {[key: ThreadId]: Thread};

export type Threads = {
	  selectedThread: ?ThreadId
	, threadsById:    ThreadMap
};

export const initialState: Threads = {
	  selectedThread:   null
	, threadsById:      {}
};

export function getThreadIdByChannelName(threads: ThreadMap, channelName: string): ?ThreadId {
	for (const k in threads) {
		const threadId = parseInt(k);
		const thread = threads[threadId];
		if (thread.info.type === "CHANNEL") {
			const channel = thread.info;
			if (channel.name === channelName) {
				return threadId;
			}
		}
	}

	return null;
};

export function getThreadByChannelName(threads: ThreadMap, channelName: string): ?Thread {
	const threadId = getThreadIdByChannelName(threads, channelName);

	if (threadId != null) {
		return threads[threadId];
	}

	return null;
};

export function getThreadIdByPmUserId(threads: ThreadMap, userId: UserId): ?ThreadId {
	for (const k in threads) {
		const threadId = parseInt(k);
		const thread = threads[threadId];
		if (thread.info.type === "PM") {
			const pm = thread.info;
			if (pm.user === userId) {
				return threadId;
			}
		}
	}

	return null;
};

export function getThreadByPmUserId(threads: ThreadMap, userId: UserId): ?Thread {
	const threadId = getThreadIdByPmUserId(threads, userId);

	if (threadId != null) {
		return threads[threadId];
	}

	return null;
};

function selectedThread(state = initialState.selectedThread, action) {
	switch (action.type) {
		case THREAD_SELECT:
			const { threadId } = action;
			return threadId;
		default:
			return state;
	}
}

function handleUsers(state = [], action) {
	switch (action.type) {
		case CHANNEL_ADD_USER: {
			const { userId } = action;
			// Ensure we don't add duplicates
			if (state.indexOf(userId) === -1) {
				return state.concat(userId);
			}
			return state;
		}
		case CHANNEL_REMOVE_USER: {
			const { userId } = action;
			const i = state.indexOf(userId);
			if (i != -1) {
				// Immutable removal
				return state.slice(0, i).concat(state.slice(i + 1));
			}
			return state;
		}
		default:
			return state;
	}
}

function threadsById(state = initialState.threadsById, action) {
	switch (action.type) {
		case CHANNEL_CREATE: {
			const { threadStatus, channelName } = action;

			const threadId = getThreadIdByChannelName(state, channelName);
			if (!threadId) {
				const newId = Object.keys(state).length;
				const newThread = {
					  threadId: newId
					, messages: []
					, status:   threadStatus

					/* Channel only */
					, info: {
							  type:     "CHANNEL"
							, users:    []
							, topic:    ""
							, name:     channelName
					}
				};
				return {
					  ...state
					, [newId]: newThread
				}
			}

			// otherwise, if threadId already exists, do nothing
			return state;
		}
		case PM_CREATE: {
			const { userId } = action;

			const threadId = getThreadIdByPmUserId(state, userId);
			if (!threadId) {
				const newId = Object.keys(state).length;
				const newThread = {
					  threadId: newId
					, messages: []
					, status:   ThreadStatus.JOINED

					/* PM only */
					, info: {
						  type:     "PM"
						, user:     userId
					}
				};
				return {
					  ...state
					, [newId]: newThread
				}
			}

			// otherwise, if threadId already exists, do nothing
			return state;
		}
		case THREAD_SET_STATUS:
		{
			const {threadId, threadStatus} = action;

			const thread = state[threadId];
			if (thread != null) {
				const newThread = {
					  ...thread
					, status: threadStatus
				};
				return {
					  ...state
					, [thread.threadId]: newThread
				}
			}

			// thread does not exist, do nothing
			return state;
		}
		case THREAD_ADD_MESSAGE:
		{
			const { threadId, messageId } = action;

			const thread = state[threadId];
			if (thread != null) {
				const messages = thread.messages.concat(messageId);
				const newThread = {...thread, messages};
				return {
					  ...state
					, [thread.threadId]: newThread
				}
			}

			// thread does not exist, do nothing
			return state;
		}
		case CHANNEL_SET_TOPIC:
		{
			const { channelName, topic } = action;

			const thread = getThreadByChannelName(state, channelName);
			if (thread != null && thread.info.type === 'CHANNEL') {
				const channel = thread.info;
				const newThread = {...thread, info: {...channel, topic}};
				return {
					  ...state
					, [thread.threadId]: newThread
				}
			}

			// thread does not exist, do nothing
			return state;
		}
		case CHANNEL_ADD_USER:
		case CHANNEL_REMOVE_USER:
		{
			const { threadId, userId } = action;

			const thread = state[threadId];
			if (thread != null && thread.info.type === 'CHANNEL') {
				const channel = thread.info;
				const users = handleUsers(channel.users, action);
				const newThread = {...thread, info: {...channel, users}};
				return {
					  ...state
					, [thread.threadId]: newThread
				}
			}

			// thread does not exist, do nothing
			return state;
		}
		default:
			return state;
	}
}

export default function threads(state: Threads = initialState, action: Action) {
	return {
		  selectedThread: selectedThread(state.selectedThread, action)
		, threadsById:    threadsById(state.threadsById, action)
	}
}
