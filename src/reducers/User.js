/*
 * @flow
 */

import {
	USER_ADD,
} from "../constants/ActionType.js"

import type {
	  Action
	, UserId
} from "../types.js";

export type Users = {[key: UserId]: string};

export default function users(state: Users = {}, action: Action): Users {
	switch (action.type) {
		case USER_ADD:
			const { userId, user } = action;
			return {
				...state,
				[userId]: user,
			}
		default:
			return state;
	}
}
