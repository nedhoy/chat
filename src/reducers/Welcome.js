/*
 * @flow
 */

import { SUBMIT_WELCOME } from "../constants/ActionType.js"

import type { Action } from "../types.js";

export type Welcome = {
	finished: boolean
};

const initialState: Welcome = {
	finished: false,
}

export default function welcome(state: Welcome = initialState, action: Action): Welcome {
	switch (action.type) {
		case SUBMIT_WELCOME:
			return {
				finished: true,
			}
		default:
			return state;
	}
}
