import io from "socket.io-client"
import config from "./config.js"

export const connect = function() {
	const uri = `https://${config.host}:${config.port}`;
	const socket = io(uri);

	return socket;
};
