/*
 * @flow
 */

import type { State } from "./reduce.js";

export type ThreadId    = number;
export type UserId      = number;
export type MessageId   = number;

// FIXME: `Message` should be typed, from IRC module
export type Message = any;

// FIXME: `action` and return type should be a union, e.g Action | ThunkAction | ...
export type Dispatch = (action: any) => any;
export type GetState = () => State;

// FIXME: `dispatch` and `getState` should have their proper types
export type Dispatcher<R> = (dispatch: Dispatch, getState: any) => R;


export type TConnectionStatus =
	| "INIT"
	| "CONNECTING"
	| "CONNECTED"
	| "DISCONNECTED"
	;

export type TIrcStatus =
	| "INIT"
	| "REGISTERED"
	| "ERROR"
	;

export type TThreadType =
	| "CHANNEL"
	| "PM"
	;

export type TThreadStatus =
	| "JOINED"
	| "PARTED"
	;

/*
 * constants
 */

/* Whether the socket is up and running or not */
export const ConnectionStatus = {
	INIT:         "INIT",
	CONNECTING:   "CONNECTING",
	CONNECTED:    "CONNECTED",
	DISCONNECTED: "DISCONNECTED",
}

/* Whether we have registered or got an error etc */
export const IrcStatus = {
	INIT:       "INIT",
	REGISTERED: "REGISTERED",
	ERROR:      "ERROR", /* e.g reconnecting too fast */
}

export const ThreadType = {
	CHANNEL: "CHANNEL",
	PM:      "PM",
}

export const ThreadStatus = {
	JOINED: "JOINED",
	PARTED: "PARTED",
}

/*
 * action types
 */

export type Action =
	/* Statuses */
	| { type: "SUBMIT_WELCOME" }
	| { type: "SET_CONNECTION_STATUS", connectionStatus: TConnectionStatus }
	| { type: "SET_IRC_STATUS", ircStatus: TIrcStatus }

	/* Nickname */
	| { type: "SET_NICKNAME", nickname: string }

	/* Threads */
	| { type: "THREAD_SELECT", threadId: ThreadId }
	| { type: "THREAD_SET_STATUS", threadId: ThreadId, threadStatus: TThreadStatus }
	| { type: "THREAD_ADD_MESSAGE", threadId: ThreadId, messageId: MessageId }

	/* Channels */
	| { type: "CHANNEL_CREATE", threadStatus: TThreadStatus, channelName: string }
	| { type: "CHANNEL_SET_TOPIC", channelName: string, topic: string }
	| { type: "CHANNEL_ADD_USER", threadId: ThreadId, userId: UserId }
	| { type: "CHANNEL_REMOVE_USER", threadId: ThreadId, userId: UserId }

	/* PMs */
	| { type: "PM_CREATE", threadStatus: TThreadStatus, userId: UserId }

	/* Messages */
	| { type: "MESSAGE_ADD", messageId: MessageId, message: Message }
	| { type: "MESSAGE_MARK_SEEN", messageIds: Array<MessageId> }

	/* Users */
	| { type: "USER_ADD", userId: UserId, user: string }

	/* Forms */
	| { type: "FORM_SHOW", formId: number, show: boolean }

	/* Composer */
	| { type: "PROMPT_SET_VALUE", threadId: ThreadId, value: string }
	| { type: "PROMPT_HISTORY_NEW", threadId: ThreadId }
	| { type: "PROMPT_HISTORY_UP", threadId: ThreadId }
	| { type: "PROMPT_HISTORY_DOWN", threadId: ThreadId }
	| { type: "PROMPT_AUTOCOMPLETE_SET_LIST", threadId: ThreadId, completions: Array<string> }
	| { type: "PROMPT_AUTOCOMPLETE_CYCLE", threadId: ThreadId }
	;
