import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import Client, {ClientEvent} from "../irc/client.js";
import {connect} from "../socket.js";
import config from "../config.js";
import ChatApp from "./ChatApp.jsx";
import WelcomePanel from "./WelcomePanel.jsx";

import {
	getMessagesFrom
} from "../persistence/persistence.js";

import {
	ConnectionStatus,
	IrcStatus,
	ThreadStatus,
	ThreadType,
} from "../types.js";

import {
	submitWelcome,
	setFormVisibility,
	setConnectionStatus,
	setIrcStatus,
	setThreadStatus,
	dispatchSelectThread,
	dispatchAddUser,
	dispatchAddPm,
	dispatchAddChannel,
	isChannel,
	getConnectionStatus,
	getIrcStatus,
	getSelectedThread,
	getSelectedThreadId,
	getThreadIdFromName,
	getThreadNameFromId,
	getChannelsWithStatus,
	getChannelUserIds,
	getUserFromId,
	dispatchMessage,
	dispatchAddMessage,
	dispatchAddMessageToThread,
	sendToSelectedChannel,
} from "../actions/actions.js";
import {
	setPromptValue,
	newPromptHistoryItem,
	historyUp,
	historyDown,
	autocomplete,
	dispatchSetAutocompleteList,
	getCompletedValue,
} from "../actions/prompt.js";

const App = React.createClass({
	getInitialState: function () {
		return {
			channelScroll: new Map(),
			reduxState: this.props.store.getState(),
			client: undefined,
			fragment: "",
		};
	},

	componentDidMount: function () {
		const unsubscribe = this.props.store.subscribe(() => this.handleStateChange());

		window.addEventListener("beforeunload", this.onBeforeUnload);

		const socket = connect();

		/**
		 * Since we connect when the page is loaded, we need to send a dummy
		 * nickname to the server to complete registration to prevent it from
		 * timing out. Once the user chooses a nickname in the welcome form, then
		 * we can update the nick.
		 */
		// FIXME: move this to onConnect, client doesn't really need to store
		//        the nickname for us, it's not going to be used.
		const randomNick = (length) => {
			const permitted = "abcdefghijklmnopqrstuvwxyz";
			let s = "";
			for (let i = 0; i < length; i++) {
				const rand = Math.floor(Math.random() * permitted.length);
				s += permitted[rand];
			}
			return s;
		};
		const nick = "rand_" + randomNick(5);
		this.state.client = new Client(socket, nick, this.props.store);

		this.state.client.on(ClientEvent.REGISTERED, this.onIrcRegistered);
		this.state.client.on(ClientEvent.ERROR, this.onIrcError);

		// Register event handlers on socket
		socket.on("connect", this.onConnect);
		socket.on("message", this.onMessage);
		socket.on("disconnect", this.onDisconnect);

		this.props.store.dispatch(setConnectionStatus(ConnectionStatus.CONNECTING));
		this.props.store.dispatch(setIrcStatus(IrcStatus.NEW));
	},

	onBeforeUnload: function(e) {
		const client = this.state.client;

		if (client) {
			client.quit("window closed");
		}
	},

	render: function () {
		let page = null;
		const connected = getConnectionStatus(this.state.reduxState);
		const ircStatus = getIrcStatus(this.state.reduxState);
		const welcomeFinished = this.state.reduxState.welcome.finished;
		const ready = connected === ConnectionStatus.CONNECTED && ircStatus === IrcStatus.REGISTERED;
		if (!welcomeFinished) {
			page = (
				<WelcomePanel
					handleSubmit={this.handleWelcomeSubmit}
					channels={config.channels}
					askChannels={config.askChannels}
					key="id_welcome"
				/>
			);
		}
		else if (!ready) {
			page = (
				<h2 key={"id_connecting"}>
					Connecting...
				</h2>
			);
		}
		else {
			const activeThread = getSelectedThread(this.state.reduxState);
			const scroll = this.state.channelScroll.get(activeThread);
			const activeThreadId = getSelectedThreadId(this.state.reduxState);
			const promptValue = getCompletedValue(this.state.reduxState, activeThreadId);
			page = (
				<ChatApp
					reduxState={this.state.reduxState}
					connected={connected === ConnectionStatus.CONNECTED}
					activeThread={activeThread}
					threadScroll={scroll}
					handleCloseThread={this.handleCloseThread}
					handleClickNick={this.handleClickNick}
					handleClickNewChannel={this.handleClickNewChannel}
					handleCloseNewChannel={this.handleCloseNewChannel}
					handleClickNewPM={this.handleClickNewPM}
					handleCloseNewPM={this.handleCloseNewPM}
					handleStartNewPM={this.handleStartNewPM}
					handleJoinChannel={this.handleJoinChannel}
					handleSend={this.handleSend}
					handlePromptChange={this.handlePromptChange}
					handleHistoryUp={this.handleHistoryUp}
					handleHistoryDown={this.handleHistoryDown}
					handleAutocomplete={this.handleAutocomplete}
					promptValue={promptValue}
					handleSelectThread={this.handleSelectThread}
					key="id_chat"
				/>
			);
		}
		return (
				<ReactCSSTransitionGroup
					transitionName="page"
					transitionEnterTimeout={500}
					transitionLeaveTimeout={300}
					>
					{page}
				</ReactCSSTransitionGroup>
		);
	},

	handleStateChange: function() {
		const state = this.props.store.getState();

		this.setState({reduxState: state});
	},

	handleCloseThread: function() {
		const threadId = getSelectedThreadId(this.state.reduxState);

		if (isChannel(this.state.reduxState, threadId)) {
			const channel = getThreadNameFromId(this.state.reduxState, threadId);
			this.state.client.part([channel]);
		}

		// FIXME: should set PARTED when we actually get the part response and
		//        instead use e.g PART_PENDING here.
		this.props.store.dispatch(setThreadStatus(threadId, ThreadStatus.PARTED));

		/*
		 * FIXME: would use `this.state.reduxState` but isn't being updated
		 *        between setting the thread status earlier and here.
		 */
		const state = this.props.store.getState();
		const channels = getChannelsWithStatus(state, ThreadStatus.JOINED);

		/* TODO: picking the first visible thread, but should do something smarter */
		const nextThreadId = channels.length > 0 ? channels[0] : null;
		this.props.store.dispatch(dispatchSelectThread(nextThreadId));
	},

	handleClickNick: function(userId) {
		console.log(userId);
		/*
		 * If there is a PM with this user then switch to it,
		 * otherwise, create a PM with this user and switch to it.
		 */
		// dispatchAddThread returns the existing threadId if a PM corresponding to
		// the userId already exists, otherwise creates the it and returns the
		// new threadId.
		const threadId = this.props.store.dispatch(dispatchAddPm(userId));
		this.props.store.dispatch(dispatchSelectThread(threadId));
		this.props.store.dispatch(setThreadStatus(threadId, ThreadStatus.JOINED)); /* Show PM */
	},

	handleClickNewChannel: function(type) {
		this.props.store.dispatch(setFormVisibility("channel", true));
	},

	handleCloseNewChannel: function() {
		this.props.store.dispatch(setFormVisibility("channel", false));
	},

	handleClickNewPM: function(type) {
		this.props.store.dispatch(setFormVisibility("pm", true));
	},

	handleCloseNewPM: function() {
		this.props.store.dispatch(setFormVisibility("pm", false));
	},

	handleStartNewPM: function(nick) {
		console.log("Start PM with: %s", nick);

		const userId = this.props.store.dispatch(dispatchAddUser(nick));
		const threadId = this.props.store.dispatch(dispatchAddPm(userId));
		this.props.store.dispatch(dispatchSelectThread(threadId));
		this.props.store.dispatch(setThreadStatus(threadId, ThreadStatus.JOINED)); /* Show PM */
	},

	handleJoinChannel: function(channel) {
		console.log("Joining: %s", channel);
		this.state.client.join([channel]);
		// TODO: autoselect channel, which means optimistically adding it
	},

	handleWelcomeSubmit: function(nick, channels) {
		// FIXME: This *can* happen before onConnect, in which might cause problems.
		console.log(`handleWelcomeSubmit(${nick}, ${channels})`);
		this.state.client.nick(nick);
		this.state.client.join(channels);

		this.props.store.dispatch(submitWelcome());

		const { host, network } = config.persistence;

		getMessagesFrom(host, network, channels)
			.then(messagesByChannel => {
				for (const channel in messagesByChannel) {
					const threadId = this.props.store.dispatch(dispatchAddChannel(channel));

					const messages = messagesByChannel[channel];
					messages.forEach(m => {
						const messageId = dispatchAddMessage(m)
							(action => this.props.store.dispatch(action));
						this.props.store.dispatch(dispatchAddMessageToThread(threadId, messageId));
					});
				}
			})
			.catch(e => console.log(e));
	},

	handleHistoryUp() {
		const activeThreadId = getSelectedThreadId(this.state.reduxState);
		this.props.store.dispatch(historyUp(activeThreadId));
	},

	handleHistoryDown() {
		const activeThreadId = getSelectedThreadId(this.state.reduxState);
		this.props.store.dispatch(historyDown(activeThreadId));
	},

	handleAutocomplete() {
		const activeThread = getSelectedThread(this.state.reduxState);

		const nickIds = getChannelUserIds(this.state.reduxState, activeThread);
		const nicks = nickIds.map(id => getUserFromId(this.state.reduxState, id));

		const activeThreadId = getSelectedThreadId(this.state.reduxState);
		this.props.store.dispatch(dispatchSetAutocompleteList(activeThreadId, nicks));
		this.props.store.dispatch(autocomplete(activeThreadId));
	},

	handlePromptChange(value) {
		const activeThreadId = getSelectedThreadId(this.state.reduxState);
		this.props.store.dispatch(setPromptValue(activeThreadId, value));
	},

	handleSend: function(value) {
		const activeThreadId = getSelectedThreadId(this.state.reduxState);
		this.props.store.dispatch(newPromptHistoryItem(activeThreadId));

		if (value.startsWith('/')) {
			const words = value.substr(1).split(' ');
			words[0] = words[0].toUpperCase();
			const command = words.join(' ');
			this.state.client._send(command);
		} else {
			const targetChannel = getSelectedThread(this.state.reduxState);
			this.state.client.privmsg(targetChannel, value);

			this.props.store.dispatch(sendToSelectedChannel(value));
		}
	},

	handleSelectThread: function(thread) {
		this.props.store.dispatch(dispatchSelectThread(thread));
	},

	onConnect: function() {
		console.log("socket.io connected");

		// FIXME: avoid accessing config from here
		this.state.client.nick(this.state.client.getNick());
		this.state.client.user(config.user, config.real);

		/**
		 * We *don't* join channels here, because we don't want the dummy nickname
		 * to be seen before we switch to the chosen nickname.
		 */

		this.props.store.dispatch(setConnectionStatus(ConnectionStatus.CONNECTED));
	},

	onMessage: function(data) {
		console.log("socket.io:message sends: " + data);
		const lines = data.split("\r\n");

		// The data is sent in chunks that can span multiple IRC messages.
		// IRC messages are terminated by \r\n and so the first and last lines
		// of the data can be fragments. For each chunk of data received, we
		// store the last line and join it to the first line of the next chunk
		// to create restore the whole message.
		lines[0] = this.state.fragment + lines[0];
		this.state.fragment = lines.splice(lines.length - 1);

		lines.forEach(line => {
			this.state.client.handleMessage(line);
			this.props.store.dispatch(dispatchMessage(line));
		});

		// Client is mutable, so we force an update
		this.forceUpdate();
	},

	onDisconnect: function() {
		console.log("socket.io disconnected");
		this.props.store.dispatch(setConnectionStatus(ConnectionStatus.DISCONNECTED));
	},

	onIrcRegistered: function() {
		console.log("irc registered");
		this.props.store.dispatch(setIrcStatus(IrcStatus.REGISTERED));
	},

	onIrcError: function(error) {
		console.log(`irc error: ${error}`);
		this.props.store.dispatch(setIrcStatus(IrcStatus.ERROR));
	},
});

export default App;
