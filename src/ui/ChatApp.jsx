import React from "react";
import onClickOutside from "react-onclickoutside";

import NickList from "./NickList.jsx";
import ThreadList from "./ThreadList.jsx";
import MessageSection from "./MessageSection.jsx";
import Icon from "./Icon.jsx";
import { ThreadStatus } from "../types.js";
import {
	getFormVisibility,
	getCurrentNick,
	getSelectedThreadId,
	getChannelsWithStatus,
	getPrivateConversationsWithStatus,
	isChannel,
	getMessagesForSelectedThread,
	getChannelTopic,
	getChannelUserIdsSorted,
} from "../actions/actions.js";

const ThreadListHeader = (props) => {
	const handleClick = (event) => {
		event.preventDefault();
		props.handleClick();
	};
	const handleKeyUp = (event) => {
		if (event.key === " ") {
			props.handleClick();
		}
	};
	return (
		<h1 className="thread-list-header">
			{props.text}
			<a
				href="#"
				className="thread-add-button"
				role="button"
				aria-label="Add thread"
				onClick={handleClick}
				onKeyUp={handleKeyUp}
			>
				<Icon name="plus" className="add-channel-button" />
			</a>
		</h1>
	);
}

class NewThreadForm extends React.Component {
	handleSubmit(event) {
		event.preventDefault();
		this.props.handleSubmit(this._input.value);
		this.props.handleClose();
	}

	handleClickOutside(event) {
		this.props.handleClose();
	}

	handleKeyUp(event) {
		if (event.key === "Escape") {
			this.props.handleClose();
		}
	}

	handleBlur(event) {
		this.props.handleClose();
	}

	render() {
		return (
			<form
				className="thread-form-new-thread"
				onSubmit={(e) => this.handleSubmit(e)}
				onKeyUp={(e) => this.handleKeyUp(e)}
				onBlur={(e) => this.handleBlur(e)}
			>
				<input
					type="text"
					role="textbox"
					aria-label={this.props.placeholder}
					placeholder={this.props.placeholder + "..."}
					ref={ref => this._input = ref}
					autoFocus
				/>
			</form>
		)
	}
}
NewThreadForm = onClickOutside(NewThreadForm);

/**
 *   +------------+-----------+--------+
 *   |  Channels  |   Topic   |        |
 *   |            +-----------+        |
 *   |............|           |        |
 *   |    PMs     |    Chat   | Users  |
 *   |            |           |        |
 *   |            +-----------+        |
 *   |            |   Entry   |        |
 *   +------------+-----------+--------+
 */

const ChatApp = (props) => {
	const activeThread = props.activeThread;
	const activeThreadId = getSelectedThreadId(props.reduxState);
	const isChannelActive = isChannel(props.reduxState, activeThreadId);
	const channels = getChannelsWithStatus(props.reduxState, ThreadStatus.JOINED);
	const pms = getPrivateConversationsWithStatus(props.reduxState, ThreadStatus.JOINED);
	const messages = getMessagesForSelectedThread(props.reduxState);
	const topic = getChannelTopic(props.reduxState, activeThread) || "";
	const nicks = getChannelUserIdsSorted(props.reduxState, activeThread) || [];
	const showNewChannelForm = getFormVisibility(props.reduxState, "channel");
	const showNewPMForm = getFormVisibility(props.reduxState, "pm");
	const newChannelForm = <NewThreadForm
		placeholder="Join channel"
		handleSubmit={props.handleJoinChannel}
		handleClose={props.handleCloseNewChannel}
	/>;
	const newPMForm = <NewThreadForm
		placeholder="Private message"
		handleSubmit={props.handleStartNewPM}
		handleClose={props.handleCloseNewPM}
	/>;
	const channelHeader = <ThreadListHeader text="Channels" handleClick={props.handleClickNewChannel} />;
	const pmHeader = <ThreadListHeader text="Private Messages" handleClick={props.handleClickNewPM} />;
	return (
		<div id="app-wrapper">
			<nav className="thread-list-section" role="navigation">
				{showNewChannelForm ? newChannelForm : channelHeader}
				<ThreadList
					reduxState={props.reduxState}
					threads={channels}
					activeThread={activeThreadId}
					handleSelectThread={props.handleSelectThread}
				/>
				{showNewPMForm ? newPMForm : pmHeader}
				<ThreadList
					reduxState={props.reduxState}
					threads={pms}
					activeThread={activeThreadId}
					handleSelectThread={props.handleSelectThread}
				/>
			</nav>
			{activeThreadId !== null
				? <MessageSection
					connected={props.connected}
					messages={messages}
					activeThread={activeThread}
					topic={topic}
					nick={getCurrentNick(props.reduxState)}
					scroll={props.threadScroll}
					handleCloseThread={props.handleCloseThread}
					handleSubmit={props.handleSend}
					handlePromptChange={props.handlePromptChange}
					handleHistoryUp={props.handleHistoryUp}
					handleHistoryDown={props.handleHistoryDown}
					handleAutocomplete={props.handleAutocomplete}
					promptValue={props.promptValue}
					handleScroll={props.handleChannelScroll}
				/>
				: "no thread selected"
			}
			{isChannelActive
				? <NickList users={nicks} reduxState={props.reduxState} handleClick={props.handleClickNick}/>
				: null}
		</div>
	);
}

export default ChatApp;
