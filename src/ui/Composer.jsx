import React from "react";

const Composer = (props) => {
	const handleSubmit = (event) => {
		event.preventDefault();
		props.handleSubmit(props.value);
	};
	const handleChange = (event) => {
		props.handleChange(event.target.value);
	};
	const handleKeyDown = (event) => {
		switch (event.key) {
			case "ArrowUp":
				props.handleUp();
				break;
			case "ArrowDown":
				props.handleDown();
				break;
			case "Tab":
				/*
				 * For accessibility reasons, only capture the Tab key
				 * when the composer is not empty.
				 */
				if (props.value.length > 0) {
					event.preventDefault();
					props.handleTab();
				}
				break;
		};
	};
	return (
		<form onSubmit={handleSubmit}>
			<input
				className="prompt"
				type="text"
				role="textbox"
				aria-label="Compose message"
				value={props.value}
				onChange={handleChange}
				onKeyDown={handleKeyDown}
				placeholder="Chat..."
			/>
		</form>
	);
}

export default Composer;
