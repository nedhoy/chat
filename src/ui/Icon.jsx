import React from "react";
import classNames from "classnames";

const icons = {
	"plus": "➕",
	"caret-bottom": "▾",
};

const Icon = ({name, className}) =>
	<span className={classNames("icon", className)}>{icons[name]}</span>;

export default Icon;
