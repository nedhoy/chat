import React from "react";

const Link = (props) => {
	return (
		<a target="_blank" {...props}>
			{props.children}
		</a>
	);
}

export default Link;

