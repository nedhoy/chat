import React from "react";
import classNames from "classnames";

export const ListItem = (props) => {
	const { className, children } = props;
	return (
		<li
			{...props}
			className={classNames("list-item", className)}
		>
			{children}
		</li>
	 );
}

export const List = (props) => {
	const { className, children } = props;
	return (
		<ul
			{...props}
			className={classNames("list", className)}
		>
			{children}
		</ul>
	);
}
