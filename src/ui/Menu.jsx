import React from "react";
import onClickOutside from "react-onclickoutside";

// FIXME: clicking a menu item does not close the menu, fixing this
//        might force us to use a JS implementation of the menu.
export const MenuItem = (props) =>
	<li
		className="menu-item"
		role="button"
		tabIndex="0"
		onClick={props.handleClick}
		onKeyUp={(e) => {if (e.key === " ") props.handleClick()}}
	>
		{props.contents}
	</li>

class Menu extends React.Component {
	handleClick(event) {
		this._menu.classList.toggle("menu-show");
	}

	handleClickOutside(event) {
		this._menu.classList.remove("menu-show");
	}

	handleClickChild() {
		this._menu.classList.remove("menu-show");
	}

	render() {
		/**
		 * We needed a way to close the menu when an item is clicked, so
		 * we augment each child's handleClick callback to close the menu.
		 */
		const children = React.Children.map(this.props.children, child => {
			return React.cloneElement(child, {
				handleClick: (e) => {
					this.handleClickChild();
					child.props.handleClick(e);
				},
			});
		});
		return (
			<div className="menu-wrapper">
				<button
					className="menu-button"
					aria-label="Menu"
					onClick={(e) => this.handleClick(e)}
				>{this.props.button}</button>
				<ul className="menu" ref={ref => this._menu = ref}>
					{children}
				</ul>
			</div>
		);
	}
}
Menu = onClickOutside(Menu);

export default Menu;
