import React from "react";
import ReactDOM from "react-dom";

import { List } from "./List.jsx";
import MessageListItem from "./MessageListItem.jsx";

const ScrolledComponent = Component => class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			/* Has the user scrolled up */
			isScrolled:     false,
		};
	}

	componentDidMount() {
		const scroll = this.props.getScroll();
		if (scroll) {
			const scrollOffset = this.scrollOffsetFrom(scroll);
			const isScrolled = scrollOffset > this.props.scrollThreshold;

			this.state.isScrolled = isScrolled;
			this.state.scroll = scroll;

			if (isScrolled) {
				this.scrollTo(scroll.scrollTop);
				return;
			}
		}

		this.scrollBottom();
	}

	componentWillUnmount() {
		if (this.state.scroll) {
			this.props.setScroll(this.state.scroll);
		}
	}

	scrollTo(scrollTop) {
		const node = ReactDOM.findDOMNode(this);
		node.scrollTop = scrollTop;
	}

	scrollBottom() {
		const node = ReactDOM.findDOMNode(this);
		node.scrollTop = node.scrollHeight;
	}

	componentDidUpdate() {
		if (!this.state.isScrolled) {
			this.scrollBottom();
		}
	}

	render() {
		return <Component
			onScroll={this.handleScroll.bind(this)}
			{...this.props}
			/>;
	}

	scrollOffsetFrom({scrollHeight, scrollTop, offsetHeight}) {
		return scrollHeight - scrollTop - offsetHeight;
	}

	handleScroll(event) {
		const node = event.target;

		const scrollOffset = this.scrollOffsetFrom(node);
		const isScrolled = scrollOffset > this.props.scrollThreshold;
		this.state.isScrolled = isScrolled;

		this.state.scroll = {
				scrollHeight: node.scrollHeight,
				scrollTop:    node.scrollTop,
				offsetHeight: node.offsetHeight,
		};
	}
}

/**
 * Smart container that keeps track of scroll position
 */
const ScrolledComponentContainer = Component => class extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			scroll: new Map(),
		};
	}

	render() {
		const key = this.props.reactKey;
		return <Component
			/**
			 * Need to provide a key so that
			 * the mount/unmount lifecycle methods fire.
			 */
			key={key}
			scrollThreshold={50}
			getScroll={this.handleGetScroll.bind(this, key)}
			setScroll={this.handleSetScroll.bind(this, key)}
			{...this.props}
			/>;
	}

	handleGetScroll(key) {
		const scroll = this.state.scroll.get(key);
		return scroll;
	}

	handleSetScroll(key, scroll) {
		this.state.scroll.set(key, scroll);
	}
}

const MessageList = (props) => {
	const rows = props.messages.map(message =>
		<MessageListItem message={message} key={message.messageId} />
	);

	return (
		<List
			className="message-list"
			role="log"
			aria-live="polite" /* redundant, maximise compatibility */
			id="message-log"
			{...props}
		>
			{rows}
		</List>
	);
};

export default ScrolledComponentContainer(ScrolledComponent(MessageList));
