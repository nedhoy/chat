import React from "react";
import classNames from "classnames";

import { ListItem } from "./List.jsx";

import { Command } from "../irc/command.js";
import { CTCPCommand, makeCTCPCommand } from "../irc/ctcp.js";
import { markupUrls, nickColour, rgbaFromHex } from "../utils.jsx";

const prefixString = function(prefix) {
	let s = "";
	if (prefix.user) {
		s += prefix.user;
		if (prefix.host) {
			s += "@" + prefix.host;
		}
	}

	return s;
};

const formatDate = (date) => {
	const hours = date.getHours();
	const minutes = date.getMinutes();

	const pad = (x) => x <= 9 ? '0' + x : x;

	return `${(hours)}:${pad(minutes)}`;
};

const MessageTimestamp = ({date}) =>
	<time className="message-timestamp">{formatDate(date)}</time>;
MessageTimestamp.propTypes = { date: React.PropTypes.instanceOf(Date).isRequired };

const MessageBody = ({text, style}) =>
	<span className="message-body" style={style}>{markupUrls(text)}</span>;

const Message = (props) => {
	const classes = classNames("message-list-item", props.className);
	return (
		<ListItem className={classes}>
			{props.children}
		</ListItem>
	);
};

const MessageChatAction = (props) => {
	const {author, text, date} = props.message;
	const colour = nickColour(author);
	const authorStyle = {
		color: colour,
	};
	return (
		<Message className="message-type-action">
			<span className="message-author" style={authorStyle}>{author}</span>
			<MessageBody text={text} />
			<MessageTimestamp date={date} />
		</Message>
	);
};

const MessageChat = (props) => {
	const {author, text, date} = props.message;
	const colour = nickColour(author);
	const authorStyle = {
		color: colour,
	};
	const bodyStyle = {
		backgroundColor: rgbaFromHex(colour, 0.2),
	};
	const isAction = props.message.ctcpType === CTCPCommand.ACTION;
	return (
		<Message className="message-type-message">
			<span className="message-author" style={authorStyle}>{author}</span>
			<MessageBody text={text} style={bodyStyle} />
			<MessageTimestamp date={date} />
		</Message>
	);
};

const MessageJoin = (props) => {
	const {prefix, nick} = props.message;
	return (
		<Message className="message-type-join">
			<span className="message-nick">{nick}</span>
			&nbsp;
			<span className="message-prefix">({prefixString(prefix)})</span>
			&nbsp;
			<span>{"has joined"}</span>
		</Message>
	);
};

const MessagePart = (props) => {
	const {prefix, nick} = props.message;
	return (
		<Message className="message-type-part">
			<span className="message-nick">{nick}</span>
			&nbsp;
			<span className="message-prefix">({prefixString(prefix)})</span>
			&nbsp;
			<span>{"has left"}</span>
		</Message>
	);
};

const MessageQuit = (props) => {
	const {prefix, nick, message} = props.message;
	return (
		<Message className="message-type-part">
			<span className="message-nick">{nick}</span>
			&nbsp;
			<span className="message-prefix">({prefixString(prefix)})</span>
			&nbsp;
			<span>has quit {message.length > 0 ? `(${message})` : null}</span>
		</Message>
	);
};

const MessageListItem = (props) => {
	const type = props.message.type;
	switch (type) {
		case Command.PRIVMSG:
			const ctcpCommand = makeCTCPCommand(props.message);
			if (ctcpCommand && ctcpCommand.type === CTCPCommand.ACTION) {
				return (
					<MessageChatAction {...props} message={ctcpCommand} />
				);
			}
			else {
				return (
					<MessageChat {...props} />
				);
			}
		case Command.JOIN:
			return (
				<MessageJoin {...props} />
			);
		case Command.PART:
			return (
				<MessagePart {...props} />
			);
		case Command.QUIT:
			return (
				<MessageQuit {...props} />
			);
		default:
			return (null);
	}
};

export default MessageListItem;
