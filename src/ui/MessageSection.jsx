import React from "react";

import MessageList from "./MessageList.jsx";
import Composer from "./Composer.jsx";
import Menu, { MenuItem } from "./Menu.jsx";
import Icon from "./Icon.jsx";
import { markupUrls } from "../utils.jsx";

const Topic = ({topic}) =>
	<div className="topic">{markupUrls(topic)}</div>

const Nick = (props) =>
	<label id="current-nickname">{props.nick}</label>

/**
 * Contains Topic, MessageList, and Entry.
 *   +-----------+
 *   |   Topic   |
 *   +-----------+
 *   |           |
 *   |    Chat   |
 *   |           |
 *   +-----------+
 *   |   Entry   |
 *   +-----------+
 */
const MessageSection = (props) => {
	const disconnectWarning =
		<div className="warning-banner" role="alert">Waiting For Connection...</div>;
	const icon = <Icon name="caret-bottom" className="menu-icon" />;
	return (
		<section className="chat-section">
			{props.connected ? null : disconnectWarning}
			<header>
				<h2 className="active-thread-name-header">
					{props.activeThread}
					<Menu button={icon}>
						<MenuItem contents="Close Channel" handleClick={props.handleCloseThread} />
					</Menu>
				</h2>
				<Topic topic={props.topic} />
			</header>
			<MessageList
				messages={props.messages}
				/**
				 * Provide a key to be used as the react `key` identifier, however
				 * we don't set `key` directly here so that the message list container
				 * persists. Internally, the key is used to force the message list
				 * to be mounted/ummounted when we switch threads.
				 */
				reactKey={props.activeThread}
			/>
			<div className="prompt-bar">
				<Nick nick={props.nick} />
				<Composer
					handleSubmit={props.handleSubmit}
					handleChange={props.handlePromptChange}
					handleUp={props.handleHistoryUp}
					handleDown={props.handleHistoryDown}
					handleTab={props.handleAutocomplete}
					value={props.promptValue}
				/>
			</div>
		</section>
	);
}

export default MessageSection;
