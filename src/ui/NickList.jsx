import React from "react";

import { List, ListItem } from "./List.jsx";

import {
	getUserFromId,
} from "../actions/actions.js";

const NickListItem = ({ user, handleClick }) =>
	<ListItem
		className="nick-list-item"
		role="button"
		tabIndex="0"
		onClick={handleClick}
		onKeyUp={(e) => {if (e.key === " ") handleClick()}}
	>
		{user}
	</ListItem>;
NickListItem.propTypes = { user: React.PropTypes.string.isRequired };

const NickList = (props) => {
	const nicks = props.users.map(userId => {
		const nick = getUserFromId(props.reduxState, userId);
		const handleClick = () => props.handleClick(userId);
		return <NickListItem user={nick} key={userId} handleClick={handleClick}/>
	});
	return (
		<div className="nick-list-section">
			<h1 className="nick-list-header">People</h1>
			<List
				className="nick-list"
				aria-live="polite"
				aria-relevant="additions removals text"
			>
				{nicks}
			</List>
		</div>
	);
};

NickList.propTypes = {
	users: React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
};

export default NickList;
