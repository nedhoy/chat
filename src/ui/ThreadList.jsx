import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import classNames from "classnames";

import { List, ListItem } from "./List.jsx";

import {
	getThreadNameFromId,
	getUnseenCountForThread,
} from "../actions/actions.js";

const Badge = (props) =>
	<span className="thread-unseen-count">{props.unseenCount}</span>;
Badge.propTypes = { unseenCount: React.PropTypes.number.isRequired };

const ThreadListItem = (props) => {
	const className = classNames({
		"thread-list-item": true,
		"active": props.active,
	});

	const badge = <Badge unseenCount={props.unseenCount} />;
	return (
		<ListItem
			className={className}
			role="button"
			tabIndex="0"
			onClick={props.handleClick}
			onKeyUp={(e) => {if (e.key === " ") props.handleClick()}}
		>
			<span className="thread-name">{props.thread}</span>
			{props.unseenCount > 0 ? badge : null}
		</ListItem>
	);
};
ThreadListItem.propTypes = {
	active:      React.PropTypes.bool.isRequired,
	unseenCount: React.PropTypes.number.isRequired,
	thread:      React.PropTypes.string.isRequired,
	handleClick: React.PropTypes.func.isRequired,
};

const ThreadList = (props) => {
	const threads = props.threads.map((thread, i) => {
		const threadName = getThreadNameFromId(props.reduxState, thread);
		const unseenCount = getUnseenCountForThread(props.reduxState, thread);
		return (
			<ThreadListItem
				key={thread}
				thread={threadName}
				active={props.activeThread === thread}
				unseenCount={unseenCount}
				handleClick={() => props.handleSelectThread(thread)}
			/>
		);
	});
	return (
		<ReactCSSTransitionGroup
			transitionName="thread"
			transitionEnterTimeout={300}
			transitionLeaveTimeout={300}
			component={List}

			className="thread-list"
			aria-live="polite"
			aria-relevant="additions removals"
		>
			{threads}
		</ReactCSSTransitionGroup>
	);
};
ThreadList.propTypes = {
	threads:            React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
	activeThread:       React.PropTypes.number.isRequired,
	handleSelectThread: React.PropTypes.func.isRequired,
};

export default ThreadList;
