import React from "react";

var WelcomePanel = React.createClass({
	handleSubmit: function (event) {
		event.preventDefault();

		const nick = this._nickField.value;

		if (this.props.askChannels) {
			/* Try to be forgiving of lost commas or missing spaces */
			const channels = this._channelsField.value;
			this.props.handleSubmit(nick, channels.split(/, *|,| /));
		}
		else {
			this.props.handleSubmit(nick, this.props.channels);
		}

	},

	render: function () {
		return (
			<section id="welcome-panel">
				<header>
					<h2>Hi, there.</h2>
					<h2>Tell us a bit about yourself.</h2>
				</header>
				<form id="welcome-form" onSubmit={this.handleSubmit}>
					<label htmlFor="nickname-field">What would you like to be called?</label>
					<input
						ref={ref => this._nickField = ref}
						id="nickname-field"
						type="text"
						placeholder="Nickname..."
						pattern="[A-Za-z[\]\\`_^{\|}]+[-A-Za-z0-9[\]\\`_^{\|}]*"
						title="Nickname should not contain spaces or start with digits"
						required
						autoFocus
					/>
					{this.props.askChannels
						? <label htmlFor="channels-field">Which channels do you want to join?</label>
						: null
					}
					{this.props.askChannels
						? <input
							ref={ref => this._channelsField = ref}
							id="channels-field"
							type="text"
							defaultValue={this.props.channels.join(', ')}
							placeholder="Channels..."
						/>
						: null
					}
					<input type="submit" value="I'm done, let me chat" />
				</form>
			</section>
		);
	}
});


export default WelcomePanel;
