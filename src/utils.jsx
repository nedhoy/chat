import React from "react";
import Link from "./ui/Link.jsx";

/**
 * Searches the string for URLs.
 *
 * The string is split at every URL, so that the components of the
 * returned array alternative between non-URLs and URLs.
 *
 * Example: findUrls("Hello https://google.com, https://reddit.com!")
 * yields [ "Hello ", "https://google.com", ", ", "https://reddit.com", "!" ].
 */
const findUrls = (s) => {
	// Match URLs
	//          <------ SCHEME ------>  ://  <-- EVERYTHING ELSE -->
	const re = /([A-Za-z][A-Za-z0-9+-.]*:\/\/[A-Za-z0-9_\-.:@#?=\/]*)/g;

	return s.split(re);
};

export const markupUrls = (s) => {
	// FIXME: some punctuation characters (e.g ? and .) used immediately
	// after a URL are captured.
	const fragments = findUrls(s);
	return fragments.map((fragment, i) => {
		if (i % 2 == 0) {
			return fragment;
		}
		else {
			return <Link href={fragment} key={i}>{fragment}</Link>;
		}
	});
};

// See http://phrogz.net/css/distinct-colors.html
// This is a tool for generating visually-distinct palettes.
const colours = [
	"#e53535",
	"#7f381d",
	"#cc6e2f",
	"#a67b26",
	"#332b0c",
	"#bfb52c",
	"#75d932",
	"#176617",
	"#0c331b",
	"#3bffbe",
	"#2cb5bf",
	"#155559",
	"#389bf2",
	"#174166",
	"#0c2133",
	"#202f8c",
	"#0f0f40",
	"#8238f2",
	"#e635c2",
	"#801d6c",
	"#f23882",
	"#731a32",
	"#330c16",
];

export const nickColour = (s) => {
	const ords = (s) => Array.from(s).map(c => c.charCodeAt());
	const sum = (xs) => xs.reduce((n, k) => n + k, 0);

	return colours[sum(ords(s)) % colours.length];
};

export const rgbaFromHex = (hex, opacity) => {
	const r = parseInt(hex.substring(1, 3), 16);
	const g = parseInt(hex.substring(3, 5), 16);
	const b = parseInt(hex.substring(5, 7), 16);

	return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};

/**
 * Find the key corresponding to the given value in the record.
 *
 * Intended for bidirectional maps, otherwise the first matching key
 * is returned.
 *
 * Returns -1 on error, so don't use that for a key.
 */
export const keyOf = (record, value) => {
	for (const key in record) {
		if (record[key] === value) {
			// Convert back to a number because we're storing the mapping as a record
			//return parseInt(key);
			return key;
		}
	}

	return -1;
}

const compare = (a, b) => {
	if (a < b) {
		return -1;
	}
	else if (a > b) {
		return 1;
	}
	return 0;
}

/**
 * Case insensitive comparison between two strings.
 */
export const compareCaseInsensitive = (a, b) => {
	return compare(a.toUpperCase(), b.toUpperCase());
}
